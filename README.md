# gitlab-ci

Library of gitlab-ci pipelines and jobs provided by Wild-Beavers.

## Usage

This section provides basic example on how to use our pipelines and jobs. For more information please read [gitlab's documentation](https://docs.gitlab.com/ee/ci/).

### Pipelines

Pipelines can be used "as-is" and provide a complete workflow for your project.

> Note: There are known limitations to gitlab-ci's include. Resolution of nested jobs and pipeline cannot go over 100 includes in total and take more then 30s.

```yaml
---
# You can define your own variables like this.
variables:
  TF_VAR_foo: $TF_VAR_foo

include:
  - project: wild-beavers/pipelines/gitlab-ci           # Path of the project
    ref: master                                         # Can be a branch, git SHA or tag
    file: pipelines/terraform-deployment.gitlab-ci.yml  # Path in the repository to the pipeline file.
```

### Jobs

Jobs can be used to create or customize your own pipeline and speed up building your own pipeline.

```yaml
---

# Other steps of the pipeline ommited

include:
  - project: wild-beavers/pipelines/gitlab-ci                   # Path of the project
    ref: master                                                 # Can be a branch, git SHA or tag
    file: jobs/semantic-release/semantic-release.gitlab-ci.yml  # Path in the repository to the pipeline file.

semantic-release:
  needs:
    - other_job
```

###  Override a container image

By default, all containers are pinned to the official image and the latest version.
In case you want to override an image or its version, you can set the appropriate input.
For example, to override the Terraform image to the `1.5` version you can set the input `terraform_container_terraform_image_tag` of the `terraform-deployment` pipeline.

```yaml
include:
  - project: wild-beavers/pipelines/gitlab-ci
    ref: 'master'
    file: 'pipelines/terraform-deployment.gitlab-ci.yml'
    inputs:
      terraform_container_terraform_image_tag: "1.5"
```

## Versioning

This repository follows [semantic versioning](https://semver.org/).

## Contributing

Please follow our [guide](CONTRIBUTING.md).
