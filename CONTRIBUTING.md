# CONTRIBUTING

Hey, first off all, thank you for contributing to this project.

## Commits

We make use of the [conventional commits format](https://www.conventionalcommits.org/).

> Note: We will strongly enforce this as we will be using the commit messages to generate our changelog and publish releases.

## pre-commit

We use [pre-commit](https://pre-commit.com/) to make some basic tests on our code before commiting.

## Conventions

In this repository, we provide jobs and complete pipelines for usage with gitlab-ci.

When developing, we make an effort to keep every job as indepedant from other jobs as possible. We also aim to provide pipelines and jobs with sane default configuration.

## Repository Structure

Repository contains two directories with gitlab-ci templates:

* jobs: Contains jobs definitions per technology that can be used to define pipelines.
* pipelines: Contains complete pipelines that can be used directly in your projects.
