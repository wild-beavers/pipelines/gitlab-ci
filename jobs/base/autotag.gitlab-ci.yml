spec:
  inputs:
    version_file:
      default: 'CHANGELOG.md'
      type: string
      # This regex force relative forward path in google RE2.
      # https://regex101.com/r/5NDc8e/2
      regex: ^((.{0,1})|(\.[^\.].*)|([^\./].*)([ -~]*[^\.]\.[^\.][ -~]*)|[^/][a-zA-Z/]+[^/])$
      description: 'Relative path of the file containing the new version to tag.'

    version_regular_expression:
      default: '^\s*\#+\s*(?<full>[0-9]+\.[0-9]+\.[0-9]+)(-[a-z]+)?.*$'
      type: string
      description: 'PCRE regular expression to extract the new version from given “inputs.version_file”. The version itself is extracted from the first capturing expression.'

    git_latest_tag_regular_expression:
      default: '^[0-9]+\.[0-9]+\.[0-9]+$'
      type: string
      description: 'Grep regular expression to extract the latest version from current project’s with git.'

    ssh_private_key_file_env_var_name:
      default: 'SSH_PRIVATE_KEY_FOR_TAGGING'
      type: string
      regex: ^[A-Z0-9_]+$
      description: 'Name of the environment variable containing the path of the SSH private key that will be used to authenticate the tagger.'

    known_hosts_file_env_var_name:
      default: 'SSH_GITLAB_KNOWN_HOST_KEYS'
      type: string
      regex: ^[A-Z0-9_]+$
      description: 'Name of the environment variable containing the path of the known host file, used to perform git push after tagging.'

    tagger_name:
      default: 'wild-beavers'
      type: string
      regex: ^[A-Za-z0-9_-]{1,64}$
      description: 'Name of the user that will tag.'

    enabled:
      default: true
      type: boolean
      description: 'Whether to enabled auto-tagging based on provided files and regular expression.'
---

.auto_tag:
  variables:
    AUTOTAG_ENABLED: $[[ inputs.enabled ]]
    VERSION_FILE: $[[ inputs.version_file ]]
  before_script:
    - LATEST_TAG_IN_CHANGELOG=$(cat $VERSION_FILE | perl -0777 -pe 's/$[[ inputs.version_regular_expression ]]/\1/s')
    - LATEST_TAG=$(git tag --sort=-v:refname| grep -E '$[[ inputs.git_latest_tag_regular_expression ]]'| head -1)

auto-tag:
  extends: .auto_tag
  stage: release
  script:
    - |
      if [ -z ${$[[ inputs.ssh_private_key_file_env_var_name ]]+x} ]; then
        echo "'$[[ inputs.ssh_private_key_file_env_var_name ]]' variable is not set (possibly this runs on non-default branch; which is a (de)bug). Impossible to tag." >&2
        exit 0
      fi
      if [[ "$LATEST_TAG_IN_CHANGELOG" == "$LATEST_TAG" ]]; then
        echo "Latest tag in changelog ($LATEST_TAG_IN_CHANGELOG) already exists" >&2
        exit 0
      fi
      git tag "$LATEST_TAG_IN_CHANGELOG"
      git config --global user.name "$[[ inputs.tagger_name ]]"
      git config --global user.email "$[[ inputs.tagger_name ]]@redacted.com"
      git remote set-url origin "git@$CI_SERVER_SHELL_SSH_HOST:$CI_PROJECT_PATH.git"
      chmod 400 "$$[[ inputs.ssh_private_key_file_env_var_name ]]"
      export GIT_SSH_COMMAND="ssh -i $$[[ inputs.ssh_private_key_file_env_var_name ]] -o IdentitiesOnly=yes -o UserKnownHostsFile=$$[[ inputs.known_hosts_file_env_var_name ]]"
      git push --tags
      unset GIT_SSH_COMMAND
  rules:
    # Runs on main branch only; only if the given version_file exists.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG == null && $AUTOTAG_ENABLED == "true"
      exists:
        - $VERSION_FILE

auto-tag-check:
  stage: static-analysis
  extends: .auto_tag
  variables:
    GIT_DEPTH: "100"
  script:
    - |
      if [[ -z "$LATEST_TAG_IN_CHANGELOG" ]]; then
        echo "Cannot fine any tag in the version file ($[[ inputs.version_file ]]). Make sure a new version is provided in $[[ inputs.version_file ]] and the regular expression is correct." >&2
        exit 1
      fi
      if [[ "$LATEST_TAG_IN_CHANGELOG" == "$LATEST_TAG" ]]; then
        echo "The latest tag found ($LATEST_TAG_IN_CHANGELOG) in the version file ($[[ inputs.version_file ]]) already exists. Make sure a new version is provided in $[[ inputs.version_file ]]." >&2
        exit 1
      fi
  rules:
    # Runs on commits only; only if the given version_file exists.
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_COMMIT_TAG == null && $AUTOTAG_ENABLED == "true"
      exists:
        - $VERSION_FILE
