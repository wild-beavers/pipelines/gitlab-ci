# container

This pipeline provides a complete container workflow.
The steps and stages are described below.

## Workflow

- static-analysis
  - pre-commit
- build
  - podman build
- publish
  - podman push

## Usage

```yaml
include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: 'pipelines/container.gitlab-ci.yml'
```
