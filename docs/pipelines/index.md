# pipelines

Pipelines are complete pipelines that can be called upon to have a standard pipeline.
We provide several pipelines for different technologies.

Pipelines are a composition of several jobs that perform a workflow designed to represent a full development cycle.

> Note: Since these are complete pipelines, they are opinionated. This means that they might not exactly work for your usecase.

## Usage

In order to have details about how to overwrite specific job options, please refer to that job's documentation.

```yaml
include:
  - project: wild-beaver/pipelines/gitlab-ci
    ref: master
    file: pipelines/terraform-deployment.gitlab-ci.yml
```

For more information, we advise you to checkout [gitlab's documentation](https://docs.gitlab.com/ee/ci/).

## Stages

Follow [WildBeavers Pipeline Library](https://gitlab.com/wild-beavers/pipelines/jenkins-pipeline-library). See: https://wild-beavers.gitlab.io/pipelines/jenkins-pipeline-library/#stages
