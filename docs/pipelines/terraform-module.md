# terraform-module

This pipeline lint, tests and release terraform modules.
The steps and stages are described below.

## Workflow

- lint
    - terraform fmt
- static-analysis
    - terraform fmt
    - terraform validate
    - terrascan (if applicable)
    - …
-
- deploy
    - terraform plan
    - terraform apply

## Usage

```yaml
include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: 'pipelines/terraform-module.gitlab-ci.yml'
```
