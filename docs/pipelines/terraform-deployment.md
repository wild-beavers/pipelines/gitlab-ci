# terraform-deployment

This pipeline provides a complete terraform workflow.
The steps and stages are described below.

## Workflow

- test
  - terraform fmt
  - terraform validate
  - repolinter (if applicable)
  - tfsec
  - gitlab-ci's secret detection job
- deploy
  - terraform plan
  - terraform apply

## Usage

```yaml
include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: 'pipelines/terraform-deployment.gitlab-ci.yml'
```
