# mkdocs

We provide several jobs for mkdocs. Each job is related to the execution context.

## mkdocs-build

Allows you to build your mkdocs site.

> Note: We expect the artifacts to be in the default mkdocs folder `site` but you can override this value.

### Usage

```yaml
stages:
  - build

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/mkdocs/build.gitlab-ci.yml
```

## mkdocs-test

Allows you to test building your mkdocs site.

> Note: This job does not run on the repository's default branch.

### Usage

```yaml
stages:
  - test

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/mkdocs/test.gitlab-ci.yml
```

## mkdocs-pages

Allows you to build and publish your mkdocs site on gitlab-pages. Only gets included in the pipeline if it's running on the default branch and if a mkdocs.yml file exists.

> Note: Your repository needs to have pages enabled in it's settings.

> Note: The build forces the `site-dir` to public for compatibility with gitlab-pages.

### Usage

```yaml
stages:
  - deploy

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/mkdocs/deploy.gitlab-ci.yml
```
