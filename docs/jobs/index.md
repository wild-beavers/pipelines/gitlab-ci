# jobs

Jobs are units of work that can be executed (almost always) independently.
We provide numerous jobs that allows you to build or extend your own pipeline.

## Usage

In general, it's pretty easy to use one of our jobs.
You only have to include it, and according to the job, you might have to personalize it to fit your need properly.

### Example

```yaml
include:
  - project: wild-beavers/pipelines/gitlab-ci
    ref: master
    file: jobs/semantic-release/semantic-release.gitlab-ci.yml

semantic-release:
  needs:
    - other_job
```

For more information, we advise you to checkout [gitlab's documentation](https://docs.gitlab.com/ee/ci/).
