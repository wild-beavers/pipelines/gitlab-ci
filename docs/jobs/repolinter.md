# pre-commit

This job will execute a `repolinter lint` on your repository.

## Usage

```yaml
stages:
  - static-analysis

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/repolinter/repolinter.gitlab-ci.yml
```

## Internals

N/A
