# semantic-release

This job will execute a `semantic-release` on your repository.

## Usage

```yaml
stages:
  - release

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/semantic-release/semantic-release.gitlab-ci.yml
```

or

```yaml
stages:
  - release

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/semantic-release/with-gpg.gitlab-ci.yml
```

## Internals

By default, only the following plugins are available:

* `@semantic-release/git`
* `@semantic-release/gitlab`
* `@semantic-release/changelog`
* `@semantic-release/commit-analyzer`
* `@semantic-release/release-notes-generator`

### semantic-release.gitlab-ci.yml

N/A

### with-gpg.gitlab-ci.yml

Allows you to run semantic-release with gpg support.

Requires the following additionnal variables to be defined:

* GPG_KEY: file variable to a gpg key
* GPG_KEY_ID: ID of the gpg key mentionned above
* GPG_PASSPHRASE: passphrase of the gpg key mentionned above
