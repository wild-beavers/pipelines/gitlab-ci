# pre-commit

This job will execute a `pre-commit run -a` on your repository.

## Usage

```yaml
stages:
  - static-analysis

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/pre-commit/pre-commit.gitlab-ci.yml
```

## Internals

This job will only get executed if a `.pre-commit-config.yaml` file exists.
It also uses a docker image that doesn't have a single external dependency installed.
If your pre-commit configuration requires it, please use your own image with all tooling installed.
