# Terraform

We provide several jobs for terraform. Each job is the equivalent of a terraform sub-command.

> Note: We do not provide a `terraform-init` job since we shouldn't use cache to transfer data from one job to another and putting the `.terraform` folder as an artifact wouldn't make sense. Each job that requires an init, will get initialized beforehand.

## terraform-apply

Allows you to run `terraform apply` on you repository.

> Note: For security reasons we do not depend on a plan provided by a terraform-plan job. This is because terraform plans contains credentials for terraform to use and exposing these as artifacts would be dangerous.

### Usage

```yaml
stages:
  - deploy

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/terraform/apply.gitlab-ci.yml
```

## terraform-fmt

Allows you to run `terraform fmt -check=true -recursive -diff` on you repository.

### Usage

```yaml
stages:
  - lint

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/terraform/fmt.gitlab-ci.yml
```

## terraform-plan

Allows you to run `terraform plan` on you repository. The job also provides gitlab with a report in order to load the terraform-widget on merge requests.

> Note: For security reasons, while we do generate a plan file for the terraform-widget in gitlab, it's not saved as an artifact since terraform plan files contain credentials. The `plan.json` report is credential free version of the terraform plan file.

### Usage

```yaml
stages:
  - build

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/terraform/plan.gitlab-ci.yml
```

## terraform-validate

Allows you to run `terraform validate` on you repository.

### Usage

```yaml
stages:
  - static-analysis

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/terraform/validate.gitlab-ci.yml
```

## terraform-test

Allows you to run `terraform test` on your repository.

> `terraform test` is still experimental in `terraform` itself.

### Usage

```yaml
stages:
  - test:integration

include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/terraform/test.gitlab-ci.yml
```

## terraform-module-publish

Allows you to publish a terraform module on your repository's infrastructure registry.

> You must enable the package registry in your repository setting for this to work.

> Note: This only run when a tag is pushed on the repository.

### Usage

```yaml
stages:
  - release
include:
  - project: wild-beavers/pipelines/gitlab-ci
    file: jobs/terraform/module-publish.gitlab-ci.yml

terraform-module-publish:
  variables:
    TERRAFORM_MODULE_DIR: <path> # Defaults to ${CI_PROJECT_DIR}
    TERRAFORM_MODULE_NAME: <module name> # Defaults to ${CI_PROJECT_NAME}
    TERRAFORM_MODULE_SYSTEM: <provider> # Defaults to undefined
    TERRAFORM_MODULE_VERSION: <semver version> # Defaults to ${CI_COMMIT_TAG}
```
