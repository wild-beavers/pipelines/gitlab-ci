# container

We provide several jobs for container (docker, podman, etc).

## container-build

Allows you to build a container.

### Usage

```yaml
stages:
  - build

include:
  - project: 'wild-beavers/pipeline/gitlab-ci'
    file: 'jobs/container/build.gitlab-ci.yml'
```

### Frequent overrides

```yaml
# […]

# Override - only build for amd64 architectures
container-default-build:
  parallel:
    matrix:
      - ARCH:
        - 'amd64'
```

```yaml
# […]

# Override - GPL 2 license
container-default-build:
    variables:
      LICENSE: 'GPL-2'
```

## container-publish

Allows you to publish containers in the project’s gitlab registry.

### Usage

```yaml
stages:
  - publish

include:
  - project: 'wild-beavers/pipeline/gitlab-ci'
    file: 'jobs/container/publish.gitlab-ci.yml'
```

### Frequent overrides

```yaml
# […]

# Override - only publish amd64 image
container-push-default:
    variables:
      PUBLISH_ARCHITECTURES: 'amd64'
```
